import Snackbar from 'react-native-snackbar';
const Snack=(props)=>{
    return Snackbar.show({
        text: props.text||'sucess',
        duration: Snackbar.LENGTH_SHORT,
      });
}
export default Snack;